This package contains hooks and scripts to keep a dracut + systemd-boot
system with Secure boot keys up-to-date and signed. It also contains a 
helper script to generate keys and install it
