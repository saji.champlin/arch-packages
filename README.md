# Arch PKGBUILD repo

This repository contains packages for setting up a system.
They are *not* AUR-friendly, and violate some rules of Arch Linux Packaging
(enabling services after install, etc). 

## Building
This repository is designed to integrate with a personal build server or
repository. Then, the packages can be installed directly without needing 
to set up an AUR helper or similar on the host machine. To aid in this,
a makefile is included that will build every package. 
